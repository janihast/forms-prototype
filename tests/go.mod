module gitlab.com/johtopilvi/wiregit/forms.git/tests

go 1.13

require (
	github.com/go-pg/pg/extra/pgdebug/v10 v10.9.3
	github.com/go-pg/pg/v10 v10.9.1
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/jackc/pgconn v1.8.1
	github.com/labstack/echo v3.3.10+incompatible
	github.com/lib/pq v1.6.0 // indirect
	github.com/onsi/ginkgo v1.16.1
	github.com/onsi/gomega v1.11.0
	github.com/pkg/errors v0.9.1
	gitlab.com/johtopilvi/wiregit/forms.git/model v0.0.0-20210522091140-15f72d8a9a3a
	gitlab.com/johtopilvi/wiregit/forms.git/rest v0.0.1
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.10
)

replace gitlab.com/johtopilvi/wiregit/forms.git/rest => ../rest

replace gitlab.com/johtopilvi/wiregit/forms.git/model => ../model
