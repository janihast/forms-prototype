package tests_database

import (
	"encoding/json"
	"fmt"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/johtopilvi/wiregit/forms.git/model"
	"gorm.io/gorm"
)

// Begin testing
var _ = Describe("session", func() {
	var (
		session *Session
		tx      *gorm.DB
	)

	// Always clear session
	BeforeEach(func() {
		err := ClearSession(&session)
		Expect(err).To(BeNil())
	}) // BeforeEach

	// Run tests in transaction so we don't need to cleanup after our selves
	BeforeEach(func() {
		tx = db.Begin()
		Expect(tx.Error).To(BeNil())
	}) // BeforeEach

	// And rollback after each tests so we do not need to cleanup after our selves
	AfterEach(func() {
		tx.Rollback()
	}) // AfterEach

	When("admin logs in", func() {
		BeforeEach(func() {
			var err error
			session, err = Login(tx, "admin")
			Expect(err).To(BeNil())
		}) // BeforeEach

		It("creates account for admin", func() {
			account := model.Account{}
			err := tx.Where("external_id = ?", session.ExternalID).First(&account).Error
			Expect(err).To(BeNil())
			Expect(account.ExternalID).To(Equal(session.ExternalID))
			Expect(account.Email).To(Equal(session.Email))
			Expect(account.ID).To(Equal(session.AccountID))
		}) // It(creates account)

		It("creates installation for admin", func() {
			installation := model.Installation{}
			err := tx.Where("domain = ?", session.Domain).First(&installation).Error
			Expect(err).To(BeNil())
			Expect(installation.Domain).To(Equal(session.Domain))
			Expect(installation.ID).To(Equal(session.InstallationID))
		}) // It(creates installation)

		When("admin logs out", func() {
			It("removes session", func() {
				err := Logout(tx, &session)
				Expect(err).To(BeNil())

				query := strings.Replace(SESSION_QUERY, "::bigint", "", -1)
				query = fmt.Sprintf("SELECT row_to_json(t.*) as data FROM (%s) AS t", query)

				r := struct {
					Data string
				}{}
				err = tx.Raw(query).First(&r).Error
				Expect(err).To(BeNil())

				var data map[string]string
				err = json.Unmarshal([]byte(r.Data), &data)
				Expect(err).To(BeNil())
				for _, v := range data {
					Expect(v).To(BeEmpty())
				}
			}) // It(removes session)
		}) // When(admin logs out)
	}) // admin logs in
}) // Describe(session)
