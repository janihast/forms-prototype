package tests_database

import (
	"math"
	"time"

	"github.com/jackc/pgconn"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/johtopilvi/wiregit/forms.git/model"
	"gorm.io/gorm"
)

var _ = Describe("table access", func() {
	var (
		session *Session
		tx      *gorm.DB
	)

	// Always clear session
	BeforeEach(func() {
		err := ClearSession(&session)
		Expect(err).To(BeNil())
	})

	// Run tests in transaction so we don't need to cleanup after our selves
	BeforeEach(func() {
		var err error
		tx = db.Begin()
		Expect(err).To(BeNil())
	})

	// And rollback after each tests so we do not need to cleanup after our selves
	AfterEach(func() {
		tx.Rollback()
	})

	Describe("account table", func() {
		When("inserting", func() {
			var (
				account model.Account
			)

			BeforeEach(func() {
				account = model.Account{
					ExternalID: "309e5f53-6d07-4c0d-b59a-00b3698e92ba",
					Email:      "someemail@test",
				}
			})

			Context("as owner", func() {
				It("inserts row", func() {
					err := tx.Create(&account).Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user", "anonymous"} {
				role := role
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("gives permission denied", func() {
						err := tx.Create(&account).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
						tx.Commit()
					})
				})
			}
		})

		When("updating", func() {
			var (
				account model.Account
			)

			BeforeEach(func() {
				account = model.Account{
					ExternalID: "309e5f53-6d07-4c0d-b59a-00b3698e92ba",
					Email:      "someemail@test",
				}
				err := tx.Create(&account).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("updates row", func() {
					err := tx.Model(&account).Update("ExternalID", "223aab3d-da6d-4711-8445-89bffb7d3fd9").Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user", "anonymous"} {
				role := role
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("gives permission denied", func() {
						err := tx.Save(&account).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			}
		})

		When("deleting", func() {
			var (
				account model.Account
			)

			BeforeEach(func() {
				account = model.Account{
					ExternalID: "309e5f53-6d07-4c0d-b59a-00b3698e92ba",
					Email:      "someemail@test",
				}
				err := tx.Create(&account).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("updates row", func() {
					err := tx.Delete(&account).Error
					Expect(err).To(BeNil())

					var account model.Account
					err = tx.First(&account, account.ID).Error
					Expect(err).To(Equal(gorm.ErrRecordNotFound))
				})
			})

			for _, role := range []string{"admin", "user", "anonymous"} {
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("gives permission denied", func() {
						err := tx.Save(&account).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			}
		})

		When("selecting", func() {
			var (
				account model.Account
			)

			BeforeEach(func() {
				account = model.Account{
					ExternalID: "309e5f53-6d07-4c0d-b59a-00b3698e92ba",
					Email:      "someemail@test",
				}
				err := tx.Create(&account).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("returns row", func() {
					err := tx.First(&account, account.ID).Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user"} {
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					Context("own account", func() {
						It("is found", func() {
							var account model.Account
							err := tx.First(&account, session.AccountID).Error
							Expect(err).To(BeNil())
						})
					})
					Context("other account", func() {
						It("is not found", func() {
							var account model.Account
							err := tx.First(&account, account.ID).Error
							Expect(err).To(Equal(gorm.ErrRecordNotFound))
						})
					})
				})
			}
			Context("as anonymous", func() {
				BeforeEach(func() {
					var err error
					session, err = LoginTo(tx, "my-test", "anonymous")
					Expect(err).To(BeNil())
				})

				It("gives permission denied", func() {
					err := tx.Where("id = ?", session.AccountID).First(&account).Error
					e := err.(*pgconn.PgError)
					Expect(e.Code).To(Equal("42501")) // permission denied for table
				})
			})
		})
	})

	Describe("installation table", func() {
		When("inserting", func() {
			Context("as owner", func() {
				var (
					installation model.Installation
				)

				BeforeEach(func() {
					installation = model.Installation{
						Domain: "my-test",
					}
					err := tx.Create(&installation).Error
					Expect(err).To(BeNil())

					err = tx.First(&installation).Error
					Expect(err).To(BeNil())
				})

				It("sets created at field", func() {
					diff := time.Now().Sub(installation.CreatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets updated at field", func() {
					diff := time.Now().Sub(installation.CreatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created by", func() {
					Expect(installation.CreatedBy).To(Equal(systemAccount.ID))
				})

				It("sets updated by", func() {
					Expect(installation.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			for _, role := range []string{"admin", "user", "anonymous"} {
				role := role
				Context("as "+role, func() {
					var (
						installation model.Installation
						err          error
					)

					BeforeEach(func() {
						var err error
						session, err = Login(tx, role)
						Expect(err).To(BeNil())
					})

					BeforeEach(func() {
						installation = model.Installation{
							Domain: "my-test",
						}
						err = tx.Create(&installation).Error
					})

					It("gives permission denied", func() {
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			}
		})

		When("updating", func() {
			var (
				installation model.Installation
			)

			BeforeEach(func() {
				installation = model.Installation{
					Domain: "my-test",
				}
				err := tx.Create(&installation).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				BeforeEach(func() {
					err := tx.Model(&installation).Update("UpdatedAt", time.Time{}).Update("UpdatedBy", 0).Error
					Expect(err).To(BeNil())

					err = tx.First(&installation).Error
					Expect(err).To(BeNil())
				})

				It("sets updated at field", func() {
					diff := time.Now().Sub(installation.CreatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets updated_by field", func() {
					Expect(installation.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			for _, role := range []string{"admin", "user", "anonymous"} {
				role := role
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("gives permission denied", func() {
						installation.Domain += "2"
						err := tx.Model(&installation).Update("Domain", installation.Domain+"2").Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			}

		})

		When("deleting", func() {
			var (
				installation model.Installation
			)

			BeforeEach(func() {
				installation = model.Installation{
					Domain: "my-test",
				}
				err := tx.Create(&installation).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("deletes row", func() {
					result := tx.Delete(&installation)
					Expect(result.Error).To(BeNil())

					err := tx.First(&installation).Error
					Expect(err).To(Equal(gorm.ErrRecordNotFound))
				})
			})

			Context("as installation admin", func() {
				BeforeEach(func() {
					var err error
					session, err = LoginTo(tx, "my-test", "admin")
					Expect(err).To(BeNil())
				})

				BeforeEach(func() {
					err := tx.Delete(&installation).Error
					Expect(err).To(BeNil())
				})

				It("dissappears from admin", func() {
					err := tx.First(&installation).Error
					Expect(err).To(Equal(gorm.ErrRecordNotFound))
				})

				Context("as a owner", func() {
					var (
						accountID uint64
					)
					BeforeEach(func() {
						accountID = session.AccountID
						err := Logout(tx, &session)
						Expect(err).To(BeNil())
					})

					BeforeEach(func() {
						err := tx.First(&installation).Error
						Expect(err).To(BeNil())
					})

					It("sets deleted_at field", func() {
						diff := time.Now().Sub(*installation.DeletedAt)
						Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
					})

					It("sets deleted_by field", func() {
						Expect(*installation.DeletedBy).To(Equal(accountID))
					})
				})

			})

			Context("as admin of another installation", func() {
				BeforeEach(func() {
					var err error
					session, err = Login(tx, "admin")
					Expect(err).To(BeNil())
				})

				BeforeEach(func() {
					err := tx.Delete(&installation).Error
					Expect(err).To(BeNil())
				})

				BeforeEach(func() {
					err := Logout(tx, &session)
					Expect(err).To(BeNil())
				})

				BeforeEach(func() {
					err := tx.First(&installation).Error
					Expect(err).To(BeNil())
				})

				It("does not set deleted_at field", func() {
					Expect(installation.DeletedAt).To(BeNil())
				})

				It("does not set deleted_by field", func() {
					Expect(installation.DeletedBy).To(BeNil())
				})
			})

			for _, role := range []string{"user", "anonymous"} {
				role := role
				Context("as "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("gives permission denied", func() {
						err := tx.Delete(&installation).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			}
		})

		When("selecting", func() {
			var (
				installation model.Installation
			)

			BeforeEach(func() {
				installation = model.Installation{
					Domain: "my-test",
				}
				err := tx.Create(&installation).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("returns row", func() {
					err := tx.First(&installation).Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user"} {
				role := role
				Context("as installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("returns row", func() {
						err := tx.First(&installation).Error
						Expect(err).To(BeNil())
					})
				})

				Context("as another installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, role)
						Expect(err).To(BeNil())
					})

					It("does not return row", func() {
						err := tx.First(&installation).Error
						Expect(err).To(Equal(gorm.ErrRecordNotFound))
					})
				})
			}

			Context("as installation anonymous", func() {
				BeforeEach(func() {
					var err error
					session, err = LoginTo(tx, "my-test", "anonymous")
					Expect(err).To(BeNil())
				})

				It("gives permission denied", func() {
					err := tx.First(&installation).Error
					e := err.(*pgconn.PgError)
					Expect(e.Code).To(Equal("42501")) // permission denied for table
				})
			})
		})
	})

	Describe("form table", func() {
		var (
			installation model.Installation
			form         model.Form
		)

		BeforeEach(func() {
			installation = model.Installation{
				Domain: "my-test",
			}
			err := tx.Create(&installation).Error
			Expect(err).To(BeNil())
		})

		BeforeEach(func() {
			form = model.Form{
				Installation: installation.ID,
				Name:         "test",
			}
		})
		Context("inserting", func() {
			Context("as owner", func() {
				BeforeEach(func() {
					err := tx.Create(&form).Error
					Expect(err).To(BeNil())

					err = tx.First(&form).Error
					Expect(err).To(BeNil())
				})

				It("sets created_at field", func() {
					diff := time.Now().Sub(form.CreatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(form.CreatedBy).To(Equal(systemAccount.ID))
				})

				It("sets updated_at field", func() {
					diff := time.Now().Sub(form.UpdatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(form.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							err := tx.Create(&form).Error
							Expect(err).To(BeNil())

							err = tx.First(&form).Error
							Expect(err).To(BeNil())
						})

						It("sets created_at field", func() {
							diff := time.Now().Sub(form.CreatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(form.CreatedBy).To(Equal(session.AccountID))
						})

						It("sets updated_at field", func() {
							diff := time.Now().Sub(form.UpdatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(form.UpdatedBy).To(Equal(session.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Create(&form).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user", "anonymous"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("inserts row", func() {
							err := tx.Create(&form).Error
							e := err.(*pgconn.PgError)
							Expect(e.Code).To(Equal("42501")) // permission denied for table
						})
					})
				}
			})
		})

		Context("updating", func() {
			var (
				origForm model.Form
			)

			BeforeEach(func() {
				err := tx.Create(&form).Error
				Expect(err).To(BeNil())

				err = tx.First(&form).Error
				Expect(err).To(BeNil())

				origForm = form
			})
			Context("as owner", func() {
				BeforeEach(func() {
					form.Name += ".2"
					form.CreatedAt = time.Time{}
					form.UpdatedAt = time.Time{}
					form.CreatedBy = 64
					form.UpdatedBy = 64

					err := tx.Save(&form).Error
					Expect(err).To(BeNil())

					err = tx.First(&form).Error
					Expect(err).To(BeNil())
				})

				It("does not let change created_at field", func() {
					Expect(form.CreatedAt).To(Equal(origForm.CreatedAt))
				})

				It("does not let change created_by field", func() {
					Expect(form.CreatedBy).To(Equal(origForm.CreatedBy))
				})

				It("sets updated_at field", func() {
					diff := time.Now().Sub(form.UpdatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(form.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							form.Name += ".2"
							form.CreatedAt = time.Time{}
							form.UpdatedAt = time.Time{}
							form.CreatedBy = 64
							form.UpdatedBy = 64

							err := tx.Save(&form).Error
							Expect(err).To(BeNil())

							err = tx.First(&form).Error
							Expect(err).To(BeNil())
						})

						It("does not let change created_at field", func() {
							Expect(form.CreatedAt).To(Equal(origForm.CreatedAt))
						})

						It("does not let change created_by field", func() {
							Expect(form.CreatedBy).To(Equal(origForm.CreatedBy))
						})

						It("sets updated_at field", func() {
							diff := time.Now().Sub(form.UpdatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(form.UpdatedBy).To(Equal(session.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Save(&form).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("does nothing", func() {
							form.Name += ".2"
							err := tx.Save(&form).Error
							e := err.(*pgconn.PgError)
							Expect(e.Code).To(Equal("42501")) // permission denied for table
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.First(&form).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})
		})

		Context("deleting", func() {
			BeforeEach(func() {
				err := tx.Create(&form).Error
				Expect(err).To(BeNil())
			})
			Context("as owner", func() {
				BeforeEach(func() {
					err := tx.Delete(&form).Error
					Expect(err).To(BeNil())

				})

				It("gets deleted", func() {
					err := tx.First(&form).Error
					Expect(err).To(Equal(gorm.ErrRecordNotFound))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						var (
							origSession *Session
						)
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
							origSession = session
						})

						BeforeEach(func() {
							err := tx.Delete(&form).Error
							Expect(err).To(BeNil())

						})

						BeforeEach(func() {
							err := Logout(tx, &session)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							err := tx.First(&form).Error
							Expect(err).To(BeNil())

						})

						It("sets deleted_at field", func() {
							diff := time.Now().Sub(*form.DeletedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets deleted_by field", func() {
							Expect(*form.DeletedBy).To(Equal(origSession.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Delete(&form).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("does nothing", func() {
							form.Name += ".2"
							err := tx.Delete(&form).Error
							Expect(err).To(BeNil())

							err = Logout(tx, &session)
							Expect(err).To(BeNil())

							err = tx.First(&form).Error
							Expect(err).To(BeNil())
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Delete(&form).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})
		})

		When("selecting", func() {
			BeforeEach(func() {
				err := tx.Create(&form).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("returns row", func() {
					err := tx.First(&form).Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user"} {
				role := role
				Context("as installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("returns row", func() {
						err := tx.First(&form).Error
						Expect(err).To(BeNil())
					})
				})

				Context("as other installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, role)
						Expect(err).To(BeNil())
					})

					It("returns row", func() {
						err := tx.First(&form).Error
						Expect(err).To(Equal(gorm.ErrRecordNotFound))
					})
				})
			}

			Context("as installation anonymous", func() {
				BeforeEach(func() {
					var err error
					session, err = LoginTo(tx, "my-test", "anonymous")
					Expect(err).To(BeNil())
				})

				It("gives permission denied", func() {
					err := tx.First(&form).Error
					e := err.(*pgconn.PgError)
					Expect(e.Code).To(Equal("42501")) // permission denied for table
				})
			})
		})
	})

	Describe("form origin table", func() {
		var (
			installation model.Installation
			form         model.Form
			formOrigin   model.FormOrigin
		)

		BeforeEach(func() {
			installation = model.Installation{
				Domain: "my-test",
			}
			err := tx.Create(&installation).Error
			Expect(err).To(BeNil())
		})

		BeforeEach(func() {
			form = model.Form{
				Installation: installation.ID,
				Name:         "test",
			}
			err := tx.Create(&form).Error
			Expect(err).To(BeNil())
		})

		BeforeEach(func() {
			formOrigin = model.FormOrigin{
				Form: form.ID,
				URL:  "http://my.test",
			}
		})

		Context("inserting", func() {
			Context("as owner", func() {
				BeforeEach(func() {
					err := tx.Create(&formOrigin).Error
					Expect(err).To(BeNil())

					err = tx.First(&formOrigin).Error
					Expect(err).To(BeNil())
				})

				It("sets created_at field", func() {
					diff := time.Now().Sub(formOrigin.CreatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(formOrigin.CreatedBy).To(Equal(systemAccount.ID))
				})

				It("sets updated_at field", func() {
					diff := time.Now().Sub(formOrigin.UpdatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(formOrigin.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							err := tx.Create(&formOrigin).Error
							Expect(err).To(BeNil())

							err = tx.First(&formOrigin).Error
							Expect(err).To(BeNil())
						})

						It("sets created_at field", func() {
							diff := time.Now().Sub(formOrigin.CreatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(formOrigin.CreatedBy).To(Equal(session.AccountID))
						})

						It("sets updated_at field", func() {
							diff := time.Now().Sub(formOrigin.UpdatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(formOrigin.UpdatedBy).To(Equal(session.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Create(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("inserts row", func() {
							err := tx.Create(&formOrigin).Error
							e := err.(*pgconn.PgError)
							Expect(e.Code).To(Equal("42501")) // permission denied for table
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Create(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})
		})
		Context("updating", func() {
			var (
				origFormOrigin model.FormOrigin
			)

			BeforeEach(func() {
				err := tx.Create(&formOrigin).Error
				Expect(err).To(BeNil())

				err = tx.First(&formOrigin).Error
				Expect(err).To(BeNil())

				origFormOrigin = formOrigin
			})
			Context("as owner", func() {
				BeforeEach(func() {
					formOrigin.URL += ".2"
					formOrigin.CreatedAt = time.Time{}
					formOrigin.UpdatedAt = time.Time{}
					formOrigin.CreatedBy = 64
					formOrigin.UpdatedBy = 64

					err := tx.Save(&formOrigin).Error
					Expect(err).To(BeNil())

					err = tx.First(&formOrigin).Error
					Expect(err).To(BeNil())
				})

				It("does not let change created_at field", func() {
					Expect(formOrigin.CreatedAt).To(Equal(origFormOrigin.CreatedAt))
				})

				It("does not let change created_by field", func() {
					Expect(formOrigin.CreatedBy).To(Equal(origFormOrigin.CreatedBy))
				})

				It("sets updated_at field", func() {
					diff := time.Now().Sub(formOrigin.UpdatedAt)
					Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
				})

				It("sets created_by field", func() {
					Expect(formOrigin.UpdatedBy).To(Equal(systemAccount.ID))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							formOrigin.URL += ".2"
							formOrigin.CreatedAt = time.Time{}
							formOrigin.UpdatedAt = time.Time{}
							formOrigin.CreatedBy = 64
							formOrigin.UpdatedBy = 64

							err := tx.Save(&formOrigin).Error
							Expect(err).To(BeNil())

							err = tx.First(&formOrigin).Error
							Expect(err).To(BeNil())
						})

						It("does not let change created_at field", func() {
							Expect(formOrigin.CreatedAt).To(Equal(origFormOrigin.CreatedAt))
						})

						It("does not let change created_by field", func() {
							Expect(formOrigin.CreatedBy).To(Equal(origFormOrigin.CreatedBy))
						})

						It("sets updated_at field", func() {
							diff := time.Now().Sub(formOrigin.UpdatedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets created_by field", func() {
							Expect(formOrigin.UpdatedBy).To(Equal(session.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Save(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("does nothing", func() {
							form.Name += ".2"
							err := tx.Save(&formOrigin).Error
							e := err.(*pgconn.PgError)
							Expect(e.Code).To(Equal("42501")) // permission denied for table
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Save(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})
		})

		Context("deleting", func() {
			BeforeEach(func() {
				err := tx.Create(&formOrigin).Error
				Expect(err).To(BeNil())
			})
			Context("as owner", func() {
				BeforeEach(func() {
					err := tx.Delete(&formOrigin).Error
					Expect(err).To(BeNil())
				})

				It("gets deleted", func() {
					err := tx.First(&formOrigin).Error
					Expect(err).To(Equal(gorm.ErrRecordNotFound))
				})
			})

			Context("as installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						var (
							origSession *Session
						)
						BeforeEach(func() {
							var err error
							session, err = LoginTo(tx, "my-test", role)
							Expect(err).To(BeNil())
							origSession = session
						})

						BeforeEach(func() {
							err := tx.Delete(&formOrigin).Error
							Expect(err).To(BeNil())

						})

						BeforeEach(func() {
							err := Logout(tx, &session)
							Expect(err).To(BeNil())
						})

						BeforeEach(func() {
							err := tx.First(&formOrigin).Error
							Expect(err).To(BeNil())

						})

						It("sets deleted_at field", func() {
							diff := time.Now().Sub(*formOrigin.DeletedAt)
							Expect(int(math.Ceil(diff.Minutes()))).To(Equal(1))
						})

						It("sets deleted_by field", func() {
							Expect(*formOrigin.DeletedBy).To(Equal(origSession.AccountID))
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Delete(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})

			Context("as non installation member", func() {
				for _, role := range []string{"admin", "user"} {
					role := role

					Context("as "+role, func() {
						BeforeEach(func() {
							var err error
							session, err = Login(tx, role)
							Expect(err).To(BeNil())
						})
						It("does nothing", func() {
							form.Name += ".2"
							err := tx.Delete(&formOrigin).Error
							Expect(err).To(BeNil())

							err = Logout(tx, &session)
							Expect(err).To(BeNil())

							err = tx.First(&formOrigin).Error
							Expect(err).To(BeNil())
						})
					})
				}

				Context("as anonymous", func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, "anonymous")
						Expect(err).To(BeNil())
					})
					It("gives permission denied", func() {
						err := tx.Save(&formOrigin).Error
						e := err.(*pgconn.PgError)
						Expect(e.Code).To(Equal("42501")) // permission denied for table
					})
				})
			})
		})

		When("selecting", func() {
			BeforeEach(func() {
				err := tx.Create(&formOrigin).Error
				Expect(err).To(BeNil())
			})

			Context("as owner", func() {
				It("returns row", func() {
					err := tx.First(&formOrigin).Error
					Expect(err).To(BeNil())
				})
			})

			for _, role := range []string{"admin", "user"} {
				role := role
				Context("as installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = LoginTo(tx, "my-test", role)
						Expect(err).To(BeNil())
					})

					It("returns row", func() {
						err := tx.First(&formOrigin).Error
						Expect(err).To(BeNil())
					})
				})

				Context("as other installation "+role, func() {
					BeforeEach(func() {
						var err error
						session, err = Login(tx, role)
						Expect(err).To(BeNil())
					})

					It("returns row", func() {
						err := tx.First(&formOrigin).Error
						Expect(err).To(Equal(gorm.ErrRecordNotFound))
					})
				})
			}

			Context("as installation anonymous", func() {
				BeforeEach(func() {
					var err error
					session, err = LoginTo(tx, "my-test", "anonymous")
					Expect(err).To(BeNil())
				})

				It("gives permission denied", func() {
					err := tx.First(&formOrigin).Error
					e := err.(*pgconn.PgError)
					Expect(e.Code).To(Equal("42501")) // permission denied for table
				})
			})
		})
	})
})
