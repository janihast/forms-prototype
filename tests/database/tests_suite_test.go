package tests_database

import (
	"encoding/json"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/pkg/errors"
	"gitlab.com/johtopilvi/wiregit/forms.git/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

var (
	db            *gorm.DB
	systemAccount model.Account
)

func TestTests(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Tests Suite")
}

var _ = BeforeSuite(func() {
	var err error
	dsn := "host=127.0.0.1 user=jani dbname=test port=5432 sslmode=disable TimeZone=Europe/Helsinki"
	db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
		Logger: logger.Default.LogMode(logger.Silent),
		//Logger: logger.Default.LogMode(logger.Info),
	})
	Expect(err).To(BeNil())
	Expect(db).ShouldNot(BeNil())

	err = db.Exec(LOGOUT_QUERY).Error
	Expect(err).To(BeNil())

	db.Where("id = session_account_id()").First(&systemAccount)
	Expect(err).To(BeNil())

	/*	truncate := `
		   DO
		   $$
		   DECLARE
			  _tbl text;
			  _sch text;
		   BEGIN
				   FOR _sch, _tbl IN
						   SELECT schemaname, tablename
						   FROM   pg_tables
						   WHERE  tablename <> 'migration' AND schemaname IN ('public') AND tableowner = current_user
				   LOOP
					   EXECUTE format('TRUNCATE TABLE %I.%I CASCADE', _sch, _tbl);
				   END LOOP;
		   END
		   $$;
		   `

		_, err = db.Exec(truncate)
		Expect(err).To(BeNil())

		_, err = db.Exec("INSERT INTO setting (key, value) VALUES ('event_account_insert_exchange', 'events')")
		_, err = db.Exec("INSERT INTO setting (key, value) VALUES ('event_account_insert_routing_key', '{installation}')")^
	*/
})

// Unmarshal event data
func ExtractEventData(e model.Event) (map[string]interface{}, error) {
	var ret map[string]interface{}
	err := json.Unmarshal([]byte(e.Data), &ret)
	if err != nil {
		return ret, errors.Wrap(err, "unable to unmarshal data")
	}

	return ret, nil
}

// Login into database
type Session struct {
	Domain         string
	ExternalID     string
	Email          string
	AccountID      uint64
	InstallationID uint64
	Role           string
}

func login(tx *gorm.DB, session *Session) error {
	var a string
	var b string
	tx.Raw("SELECT txid_current()").First(&a)
	tx.Raw("SELECT txid_current()").First(&b)

	if a != b {
		return errors.New("is not in transaction\n")
	}

	l := model.Login{}
	err := tx.Raw(LOGIN_QUERY, session.Domain, session.ExternalID, session.Email, session.Role).Scan(&l).Error
	if err != nil {
		return errors.Wrap(err, "unable to login")
	}

	session.AccountID = l.AccountID
	session.InstallationID = l.InstallationID

	var r string
	if result := tx.Raw("SELECT current_setting('role')").First(&r); result.Error != nil {
		return errors.Wrap(result.Error, "role fetch failed")
	} else if r != session.Role {
		return errors.New("role mismatch")
	}

	var aid uint64
	if result := tx.Raw("SELECT session_account_id()").First(&aid); result.Error != nil {
		return errors.Wrap(result.Error, "account id fetch failed")
	} else if aid != session.AccountID {
		return errors.New("account id mismatch")
	}

	var iid uint64
	if result := tx.Raw("SELECT session_installation_id()").First(&iid); result.Error != nil {
		return errors.Wrap(result.Error, "installation id fetch failed")
	} else if iid != session.InstallationID {
		return errors.New("installation id mismatch")
	}

	return nil
}

func Login(tx *gorm.DB, role string) (*Session, error) {
	session := &Session{}
	session.Domain = installationUniqueDomain()
	session.ExternalID = accountUniqueExternalID()
	session.Email = accountUniqueEmail()
	session.Role = role

	return session, login(tx, session)
}

func LoginTo(tx *gorm.DB, domain string, role string) (*Session, error) {
	session := &Session{}
	session.Domain = domain
	session.ExternalID = accountUniqueExternalID()
	session.Email = accountUniqueEmail()
	session.Role = role

	return session, login(tx, session)
}

func ReLoginAsNewUser(tx *gorm.DB, session *Session, role string) error {
	session.ExternalID = accountUniqueExternalID()
	session.Email = accountUniqueEmail()
	session.Role = role

	return login(tx, session)
}

func Logout(tx *gorm.DB, session **Session) (err error) {
	err = tx.Exec(LOGOUT_QUERY).Error
	*session = nil

	return
}

func ClearSession(session **Session) error {
	return Logout(db, session)
}
