package tests_database

// Login query
const LOGIN_QUERY = "SELECT o_account_id as account_id, o_installation_id as installation_id FROM login(?, ?, ?, ?)"

// Logout query
const LOGOUT_QUERY = "SELECT logout()"

// Session query
const SESSION_QUERY = "SELECT current_setting('wg.installation_id')::bigint as installation_id, current_setting('wg.account_id')::bigint as account_id"
