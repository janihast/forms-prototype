package tests_database

import "fmt"

// Unique account external id
var counterAccountExternalID int

func accountUniqueExternalID() string {
	counterAccountExternalID++
	return fmt.Sprintf("external-id:%d", counterAccountExternalID)
}

// Unique account email
var counterAccountEmail int

func accountUniqueEmail() string {
	counterAccountEmail++
	return fmt.Sprintf("email+%d@localhost", counterAccountEmail)
}

// Unique installation domain
var counterInstallationDomain int

func installationUniqueDomain() string {
	counterInstallationDomain++
	return fmt.Sprintf("test-%d.localhost", counterInstallationDomain)
}
