package tests_actions

import (
	"net/http/httptest"

	"github.com/labstack/echo"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/johtopilvi/wiregit/forms.git/rest/actions"
)

var _ = Describe("the strings package", func() {
	var (
		e *echo.Echo
	)
	BeforeEach(func() {
		e, _ = actions.App()
	})
	Context("strings.Contains()", func() {
		When("the string contains the substring in the middle", func() {
			It("returns `true`", func() {
				req := httptest.NewRequest("GET", "/health", nil)

				rec := httptest.NewRecorder()
				e.ServeHTTP(rec, req)

				Expect(rec.Result().StatusCode).Should(Equal(200))
				Expect(rec.Body.String()).Should(Equal("OK!"))
			})
		})
	})
})
