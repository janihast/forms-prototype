----------------------- Functions --------------------------
CREATE OR REPLACE FUNCTION session_account_id() RETURNS BIGINT AS $$
DECLARE
	d_account_id VARCHAR;
BEGIN

	BEGIN
		SELECT current_setting('wg.account_id') INTO d_account_id;
	EXCEPTION
		WHEN SQLSTATE '42704' THEN
			d_account_id := null;
	END;

	IF d_account_id IS NULL OR d_account_id = '' THEN
		SELECT value INTO d_account_id FROM setting WHERE key = 'system_account_id';
		IF NOT FOUND THEN
			RAISE EXCEPTION 'system account external id not found';
		END IF;
	END IF;

	RETURN d_account_id::BIGINT;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION session_account_id IS 'Gets account id from session. Defaults to system account id.';

CREATE OR REPLACE FUNCTION session_installation_id() RETURNS BIGINT AS $$
DECLARE
	d_installation_id VARCHAR;
BEGIN

	BEGIN
		SELECT current_setting('wg.installation_id') INTO d_installation_id;
	EXCEPTION
		WHEN SQLSTATE '42704' THEN
			d_installation_id := null;
	END;

	IF d_installation_id IS NULL OR d_installation_id = '' THEN
		SELECT value INTO d_installation_id FROM setting WHERE key = 'system_installation_id';
		IF NOT FOUND THEN
			RAISE EXCEPTION 'system installation external id not found';
		END IF;
	END IF;

	RETURN d_installation_id::BIGINT;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION session_installation_id IS 'Gets installation id from session. Defaults to system installation id.';

CREATE OR REPLACE FUNCTION default_before_insert() RETURNS TRIGGER AS $$
DECLARE
	account_id BIGINT;
BEGIN

	SELECT session_account_id() INTO account_id;

	NEW.created_at = clock_timestamp();
	NEW.updated_at = clock_timestamp();
	NEW.created_by = account_id;
	NEW.updated_by = account_id;

	RETURN NEW;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION default_before_insert IS 'sets created/updated fields on table insert';

CREATE OR REPLACE FUNCTION default_before_update() RETURNS TRIGGER AS $$
DECLARE
	account_id BIGINT;
BEGIN
	SELECT session_account_id() INTO account_id;

	NEW.created_at = OLD.created_at;
	NEW.created_by = OLD.created_by;
	NEW.updated_at = clock_timestamp();
	NEW.updated_by = account_id;

	RETURN NEW;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION default_before_update IS 'sets updated fields on table update and does not allow created field change';

CREATE OR REPLACE FUNCTION default_before_delete() RETURNS TRIGGER AS $$
DECLARE
	role VARCHAR;
	account_id BIGINT;
BEGIN
	SELECT current_setting('role') INTO role;
	IF role = 'none' THEN
		RETURN OLD;
	END IF;

	SELECT session_account_id() INTO account_id;

	EXECUTE format('UPDATE %I.%I SET deleted_at = clock_timestamp(), deleted_by = %L WHERE id = %L', TG_TABLE_SCHEMA, TG_TABLE_NAME, account_id, OLD.id);

	RETURN NULL;
END; $$ SECURITY DEFINER LANGUAGE plpgsql;
COMMENT ON FUNCTION default_before_delete IS 'sets deleted fields on table delete for users but owner gets row deleted';

CREATE OR REPLACE FUNCTION default_after_operation() RETURNS TRIGGER AS $$
BEGIN
	PERFORM send_event(concat(TG_TABLE_NAME, '_', lower(TG_OP)), json_build_object('old', OLD, 'new', NEW));
	RETURN NEW;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION default_after_operation IS 'sends event of table operation(insert, update, delete)';

CREATE OR REPLACE FUNCTION login_init (p_domain VARCHAR, p_external_id VARCHAR, p_email VARCHAR, p_role VARCHAR, out o_installation_id BIGINT, out o_account_id BIGINT) AS $$
BEGIN
	SELECT id INTO o_account_id FROM account WHERE external_id = p_external_id;
	IF NOT FOUND THEN
		INSERT INTO account (external_id, email) VALUES (p_external_id, p_email) RETURNING id INTO o_account_id;
	ELSE
		UPDATE account SET email = p_email WHERE external_id = p_external_id;
	END IF;

	SELECT id INTO o_installation_id FROM installation WHERE domain = p_domain AND deleted_at IS NULL;
	IF NOT FOUND THEN
		INSERT INTO installation (domain, created_by, updated_by) VALUES (p_domain, o_account_id, o_account_id) RETURNING id INTO o_installation_id;
	END IF;
END; -- end of login
$$ SECURITY DEFINER LANGUAGE plpgsql;
COMMENT ON FUNCTION login_init IS 'Creates installation and account and is separated from login because it needs more privileges than logged in user has';

CREATE OR REPLACE FUNCTION login (p_domain VARCHAR, p_external_id VARCHAR, p_email VARCHAR, p_role VARCHAR, out o_account_id BIGINT, out o_installation_id BIGINT) AS $$
DECLARE
	rec RECORD;
BEGIN
	rec := login_init(p_domain, p_external_id, p_email, p_role);

	EXECUTE format('SET ROLE %I', p_role);

	SELECT rec.o_account_id INTO o_account_id;
	SELECT rec.o_installation_id INTO o_installation_id;
	PERFORM set_config('wg.account_id', o_account_id::TEXT, false);
	PERFORM set_config('wg.installation_id', o_installation_id::TEXT, false);
END; -- end of login
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION login IS 'creates the session, sets the role and limits visibility of data';

CREATE OR REPLACE FUNCTION logout () RETURNS BOOLEAN AS $$
DECLARE
	i VARCHAR;
	installation_id VARCHAR;
	account_id VARCHAR;
BEGIN
	RESET ROLE;
	FOREACH i IN ARRAY '{"wg.installation_id","wg.account_id"}'::VARCHAR[]
	LOOP
		PERFORM set_config(i, null, false);
	END LOOP;

	RETURN true;
END; -- end of logout
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION logout IS 'Removes session and returns to original user role(owner)';

CREATE FUNCTION event_after_insert() RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
	NOTIFY event;
	RETURN new;
END;
$$;
COMMENT ON FUNCTION event_after_insert IS 'Sends notification to `event` channel when new row is added to event table';

CREATE FUNCTION send_event(p_type VARCHAR, p_data JSON) RETURNS BOOLEAN AS $$
DECLARE
	account_id BIGINT;
	installation_id BIGINT;
	exchange VARCHAR;
	routing_key VARCHAR;
BEGIN
	SELECT session_account_id() INTO account_id;
	SELECT session_installation_id() INTO installation_id;
	SELECT value INTO exchange FROM setting WHERE key = 'event_account_insert_exchange';
	SELECT value INTO routing_key FROM setting WHERE key = 'event_account_insert_routing_key';
	routing_key = replace(routing_key, '{installation}', installation_id::VARCHAR);
	INSERT INTO event ("type", exchange, routing_key, data, created_by)
		VALUES (p_type, exchange, routing_key, p_data, account_id);
	RETURN true;
END; $$ SECURITY DEFINER LANGUAGE plpgsql;
COMMENT ON FUNCTION send_event IS 'Send system event';

CREATE OR REPLACE FUNCTION event_before_insert() RETURNS TRIGGER AS $$
DECLARE
	account_id BIGINT;
BEGIN
	SELECT session_account_id() INTO account_id;

	NEW.created_by = account_id;

	RETURN NEW;
END; $$ LANGUAGE plpgsql;
COMMENT ON FUNCTION event_before_insert IS 'sets created fields on event table insert';

----------------------- Roles --------------------------

DO $$
DECLARE
	role VARCHAR;
BEGIN
	FOREACH role IN ARRAY '{"admin","user","anonymous"}'::VARCHAR[]
	LOOP
		PERFORM FROM pg_roles WHERE rolname = role;
		IF FOUND THEN
			EXECUTE format('DROP ROLE %I', role);
		END IF;
		EXECUTE format('CREATE ROLE %I', role);
	END LOOP;
	COMMENT ON ROLE admin IS 'Admin of the installation';
	COMMENT ON ROLE "user" IS 'User of the installation';
	COMMENT ON ROLE anonymous IS 'Random peer who can make new entries';
END; $$ LANGUAGE plpgsql;

----------------------- Tables --------------------------

CREATE TABLE setting (
	id BIGSERIAL NOT NULL,
	key VARCHAR NOT NULL,
	value VARCHAR NOT NULL,
	CONSTRAINT pk_setting PRIMARY KEY (id)
);
COMMENT ON TABLE setting IS 'System settings are found from here';
COMMENT ON COLUMN setting.id IS 'Record identifier';
COMMENT ON COLUMN setting.key IS 'Setting key';
COMMENT ON COLUMN setting.value IS 'Actual value of setting';

CREATE TABLE account (
	id BIGSERIAL NOT NULL,
	external_id VARCHAR NOT NULL,
	email VARCHAR NOT NULL,

	CONSTRAINT pk_account PRIMARY KEY (id),
	CONSTRAINT uq_account UNIQUE (external_id)
);
COMMENT ON TABLE account IS 'Accounts who logged in';
COMMENT ON COLUMN account.id IS 'Record identifier';
COMMENT ON COLUMN account.external_id IS 'Identifier given by external authority. Eg. keycloak';
COMMENT ON COLUMN account.email IS 'Account email address';

CREATE TABLE event (
	id UUID NOT NULL DEFAULT gen_random_uuid(),
	"type" VARCHAR NOT NULL,
	exchange VARCHAR NOT NULL,
	routing_key VARCHAR NOT NULL,
	data JSON NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	created_by BIGINT NOT NULL,

	CONSTRAINT pk_event PRIMARY KEY (id)
);
COMMENT ON TABLE event IS 'Event table is temporary place for system events';
COMMENT ON COLUMN event.id IS 'Record identifier';
COMMENT ON COLUMN event."type" IS 'Record identifier';
COMMENT ON COLUMN event.exchange IS 'AMQP exchange to send data to';
COMMENT ON COLUMN event.routing_key IS 'AMQP routing key to send data with';
COMMENT ON COLUMN event.data IS 'Data of the event';
COMMENT ON COLUMN event.created_at IS 'When the record was created';
COMMENT ON COLUMN event.created_by IS 'Who created the record';

CREATE TABLE installation (
	id BIGSERIAL NOT NULL,
	domain VARCHAR NOT NULL,

	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	created_by BIGINT NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	updated_by BIGINT NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE,
	deleted_by BIGINT,

	CONSTRAINT pk_installation PRIMARY KEY (id),
	CONSTRAINT uq_installation_domain UNIQUE (domain)
);
COMMENT ON TABLE installation IS 'Installation represent the instance of software for customer. Eg. nukkekotiin.fi.';
COMMENT ON COLUMN installation.id IS 'Record identifier';
COMMENT ON COLUMN installation.domain IS 'domain where installation resides';
COMMENT ON COLUMN installation.created_at IS 'When the record was created';
COMMENT ON COLUMN installation.created_by IS 'Who created the record';
COMMENT ON COLUMN installation.updated_at IS 'When the record was last updated';
COMMENT ON COLUMN installation.updated_by IS 'Who updated the record';
COMMENT ON COLUMN installation.deleted_at IS 'When the record was deleted';
COMMENT ON COLUMN installation.deleted_by IS 'Who deleted the record';

CREATE TABLE form (
	id BIGSERIAL NOT NULL,
	name VARCHAR NOT NULL,
	installation BIGINT NOT NULL DEFAULT session_installation_id(),
	external_name UUID NOT NULL DEFAULT gen_random_uuid(),
	enabled BOOLEAN NOT NULL DEFAULT false, 
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	created_by BIGINT NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	updated_by BIGINT NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE,
	deleted_by BIGINT,

	CONSTRAINT pk_form PRIMARY KEY (id),
	CONSTRAINT fk_form_installation FOREIGN KEY (installation) REFERENCES installation(id) ON UPDATE CASCADE ON DELETE CASCADE
);
COMMENT ON TABLE form IS 'Represents sendable forms out there. Eg. contact from in johtopilvi.fi. Forms can be managed by admins and users';
COMMENT ON COLUMN form.id IS 'Record identifier';
COMMENT ON COLUMN form.name IS 'Form name';
COMMENT ON COLUMN form.installation IS 'Installation where form belongs to';
COMMENT ON COLUMN form.external_name IS 'UUID field to user for public form sending address';
COMMENT ON COLUMN form.enabled IS 'If form accepts data in';
COMMENT ON COLUMN form.created_at IS 'When the record was created';
COMMENT ON COLUMN form.created_by IS 'Who created the record';
COMMENT ON COLUMN form.updated_at IS 'When the record was last updated';
COMMENT ON COLUMN form.updated_by IS 'Who updated the record';
COMMENT ON COLUMN form.deleted_at IS 'When the record was deleted';
COMMENT ON COLUMN form.deleted_by IS 'Who deleted the record';

CREATE TABLE form_origin (
	id BIGSERIAL NOT NULL,
	form BIGINT NOT NULL,
	url VARCHAR NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	created_by BIGINT NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	updated_by BIGINT NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE,
	deleted_by BIGINT,

	CONSTRAINT pk_form_origin PRIMARY KEY (id),
	CONSTRAINT fk_form_origin_form FOREIGN KEY (form) REFERENCES form(id) ON UPDATE CASCADE ON DELETE CASCADE
);
COMMENT ON TABLE form_origin IS 'CORS policy origin for form. Origins can be managed by admins and users';
COMMENT ON COLUMN form_origin.id IS 'Record identifier';
COMMENT ON COLUMN form_origin.form IS 'Form where origin belongs to';
COMMENT ON COLUMN form_origin.url IS 'URL of origin';
COMMENT ON COLUMN form_origin.created_at IS 'When the record was created';
COMMENT ON COLUMN form_origin.created_by IS 'Who created the record';
COMMENT ON COLUMN form_origin.updated_at IS 'When the record was last updated';
COMMENT ON COLUMN form_origin.updated_by IS 'Who updated the record';
COMMENT ON COLUMN form_origin.deleted_at IS 'When the record was deleted';
COMMENT ON COLUMN form_origin.deleted_by IS 'Who deleted the record';

CREATE TABLE form_entry (
	id BIGSERIAL NOT NULL,
	form BIGINT NOT NULL,
	client_ip INET NOT NULL,
	client_user_agent VARCHAR NOT NULL,
	data JSON NOT NULL,

	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	created_by BIGINT NOT NULL,
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT clock_timestamp(),
	updated_by BIGINT NOT NULL,
	deleted_at TIMESTAMP WITH TIME ZONE,
	deleted_by BIGINT,

	CONSTRAINT pk_form_entry PRIMARY KEY (id),
	CONSTRAINT fk_form_entry_form FOREIGN KEY (form) REFERENCES form (id) ON UPDATE CASCADE ON DELETE CASCADE
);
COMMENT ON TABLE form_entry IS 'Entries sent to form. Entries can be managed by admin and users.';
COMMENT ON COLUMN form_entry.id IS 'Record identifier';
COMMENT ON COLUMN form_entry.form IS 'Form where data was sent to';
COMMENT ON COLUMN form_entry.client_ip IS 'IP where the data was sent from';
COMMENT ON COLUMN form_entry.client_user_agent IS 'User agent of and user who sent the data';
COMMENT ON COLUMN form_entry.data IS 'Data that was sent';
COMMENT ON COLUMN form_entry.created_at IS 'When the record was created';
COMMENT ON COLUMN form_entry.created_by IS 'Who created the record';
COMMENT ON COLUMN form_entry.updated_at IS 'When the record was last updated';
COMMENT ON COLUMN form_entry.updated_by IS 'Who updated the record';
COMMENT ON COLUMN form_entry.deleted_at IS 'When the record was deleted';
COMMENT ON COLUMN form_entry.deleted_by IS 'Who deleted the record';

----------------------- Grant --------------------------

GRANT SELECT ON setting TO admin;
GRANT SELECT ON setting TO "user";
GRANT SELECT ON setting TO anonymous;

GRANT SELECT ON account TO admin;
GRANT SELECT ON account TO "user";

GRANT SELECT,DELETE ON installation TO admin;
GRANT SELECT ON installation TO "user";

GRANT SELECT,INSERT,UPDATE,DELETE ON form TO admin;
GRANT SELECT,INSERT,UPDATE,DELETE ON form TO "user";
GRANT USAGE ON SEQUENCE form_id_seq TO admin;
GRANT USAGE ON SEQUENCE form_id_seq TO "user";

GRANT SELECT,INSERT,UPDATE,DELETE ON form_origin TO admin;
GRANT SELECT,INSERT,UPDATE,DELETE ON form_origin TO "user";
GRANT USAGE ON SEQUENCE form_origin_id_seq TO admin;
GRANT USAGE ON SEQUENCE form_origin_id_seq TO "user";

GRANT SELECT, DELETE ON form_entry to admin;
GRANT SELECT ON form_entry to "user";

----------------------- RLS --------------------------

ALTER TABLE account ENABLE ROW LEVEL SECURITY;
CREATE POLICY account_select_policy ON account FOR SELECT
	USING (
		current_setting('role') = 'none'
		OR id = session_account_id()
	);

ALTER TABLE installation ENABLE ROW LEVEL SECURITY;
CREATE POLICY installation_select_policy ON installation FOR SELECT
	USING (
		current_setting('role') = 'none'
		OR (id = session_installation_id() AND deleted_at IS NULL)
	);
CREATE POLICY installation_delete_policy ON installation FOR DELETE
	USING (
		current_setting('role') = 'none'
		OR (id = session_installation_id() AND current_setting('role') = 'admin')
	);

ALTER TABLE form ENABLE ROW LEVEL SECURITY;
CREATE POLICY form_all_policy ON form FOR ALL
	USING (
		current_setting('role') = 'none'
		OR (
			installation = session_installation_id()
			AND deleted_at IS NULL
		)
	);

ALTER TABLE form_origin ENABLE ROW LEVEL SECURITY;
CREATE POLICY form_origin_all_policy ON form_origin FOR ALL
	USING (
		current_setting('role') = 'none'
		OR (
			EXISTS (SELECT 1 FROM form f WHERE f.installation = session_installation_id() AND f.id = form_origin.form)
			AND deleted_at IS NULL
		)
	);

ALTER TABLE form_entry ENABLE ROW LEVEL SECURITY;
CREATE POLICY form_entry_all_policy ON form_entry FOR ALL
	USING (
		current_setting('role') = 'none' OR
		EXISTS (SELECT 1 FROM form f WHERE f.installation = session_installation_id() AND f.id = form_entry.form)
	);

----------------------- Triggers --------------------------

CREATE TRIGGER event_before_insert_trg BEFORE INSERT ON event FOR EACH STATEMENT EXECUTE FUNCTION event_before_insert();
CREATE TRIGGER event_after_insert_trg AFTER INSERT ON event FOR EACH STATEMENT EXECUTE FUNCTION event_after_insert();

CREATE TRIGGER installation_before_insert_trg BEFORE INSERT ON installation FOR EACH ROW EXECUTE PROCEDURE default_before_insert();
CREATE TRIGGER installation_after_insert_trg AFTER INSERT ON installation FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER installation_before_update_trg BEFORE UPDATE ON installation FOR EACH ROW EXECUTE PROCEDURE default_before_update();
CREATE TRIGGER installation_after_update_trg AFTER UPDATE ON installation FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER installation_before_delete_trg BEFORE DELETE ON installation FOR EACH ROW EXECUTE PROCEDURE default_before_delete();
CREATE TRIGGER installation_after_delete_trg AFTER DELETE ON installation FOR EACH ROW EXECUTE PROCEDURE default_after_operation();

CREATE TRIGGER form_before_insert_trg BEFORE INSERT ON form FOR EACH ROW EXECUTE PROCEDURE default_before_insert();
CREATE TRIGGER form_after_insert_trg AFTER INSERT ON form FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_before_update_trg BEFORE UPDATE ON form FOR EACH ROW EXECUTE PROCEDURE default_before_update();
CREATE TRIGGER form_after_update_trg AFTER UPDATE ON form FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_before_delete_trg BEFORE DELETE ON form FOR EACH ROW EXECUTE PROCEDURE default_before_delete();
CREATE TRIGGER form_after_delete_trg AFTER DELETE ON form FOR EACH ROW EXECUTE PROCEDURE default_after_operation();

CREATE TRIGGER form_origin_before_insert_trg BEFORE INSERT ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_before_insert();
CREATE TRIGGER form_origin_after_insert_trg AFTER INSERT ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_origin_before_update_trg BEFORE UPDATE ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_before_update();
CREATE TRIGGER form_origin_after_update_trg AFTER UPDATE ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_origin_before_delete_trg BEFORE DELETE ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_before_delete();
CREATE TRIGGER form_origin_after_delete_trg AFTER DELETE ON form_origin FOR EACH ROW EXECUTE PROCEDURE default_after_operation();

CREATE TRIGGER form_entry_before_insert_trg BEFORE INSERT ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_before_insert();
CREATE TRIGGER form_entry_after_insert_trg AFTER INSERT ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_entry_before_update_trg BEFORE UPDATE ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_before_update();
CREATE TRIGGER form_entry_after_update_trg AFTER UPDATE ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_after_operation();
CREATE TRIGGER form_entry_before_delete_trg BEFORE DELETE ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_before_delete();
CREATE TRIGGER form_entry_after_delete_trg AFTER DELETE ON form_entry FOR EACH ROW EXECUTE PROCEDURE default_after_operation();

----------------------- Indexes --------------------------

CREATE INDEX ind_account_external_id ON account(external_id);

CREATE INDEX ind_form_installation ON form(installation);
CREATE INDEX ind_form_installation_external_name ON form(installation, external_name);

CREATE INDEX ind_form_origin_form ON form_origin(form);
CREATE INDEX ind_form_origin_url ON form_origin(url);

CREATE INDEX ind_form_entry_form ON form_entry(form);
CREATE INDEX ind_form_entry_form_created_at ON form_entry(form, created_at);

CREATE INDEX ind_installation_domain ON installation (domain);

CREATE INDEX ind_setting_key ON setting(key);

----------------------- Default data --------------------------

DO $$
DECLARE
	d_system_account_id BIGINT;
	d_system_installation_id BIGINT;
	d_anonymous_account_id BIGINT;
BEGIN
	SELECT nextval('account_id_seq') INTO d_system_account_id;
	SELECT nextval('account_id_seq') INTO d_anonymous_account_id;
	SELECT nextval('installation_id_seq') INTO d_system_installation_id;

	INSERT INTO setting (key, value) VALUES ('event_account_insert_exchange', 'events');
	INSERT INTO setting (key, value) VALUES ('event_account_insert_routing_key', '{installation}');
	INSERT INTO setting (key, value) VALUES ('system_account_id', d_system_account_id::VARCHAR);
	INSERT INTO setting (key, value) VALUES ('system_installation_id', d_system_installation_id::VARCHAR);

	INSERT INTO account(id, external_id, email) VALUES (d_system_account_id, '94835b7e-2345-47b4-98ac-4db2d07cbce4', 'system@local');
	INSERT INTO account(id, external_id, email) VALUES (d_anonymous_account_id, '8d4d133a-08fd-4a00-bf6c-28887b198984', 'anonymous@local');

	INSERT INTO installation(id, domain) VALUES (d_system_installation_id, 'system');
END; $$ LANGUAGE plpgsql;
