# syntax=docker/dockerfile:1.0.0-experimental
# Build database application

# STAGE ca: Fetch the ca certificates
FROM alpine:3.13.5 as ca

RUN apk add --no-cache ca-certificates
RUN update-ca-certificates

# Fetch the Golang dependencies
FROM golang:1.16.4 as dependencies
ENV GO111MODULE=on
ENV GOPROXY="https://proxy.golang.org"
ENV GONOSUMDB="gitlab.com"
ENV GOPRIVATE="gitlab.com"

COPY * /tmp/puup/
RUN ls -la /tmp/puup/

WORKDIR /sources
COPY go.* .

RUN git config --global url.ssh://git@gitlab.com/.insteadOf https://gitlab.com/
RUN mkdir ~/.ssh && ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
RUN --mount=type=ssh go mod download

# STAGE build: Build the Goland application
FROM golang:1.16.4 as build
ENV GO111MODULE=on
ENV GOPROXY="https://proxy.golang.org"
ENV GONOSUMDB="gitlab.com"
ENV GOPRIVATE="gitlab.com"

COPY --from=dependencies /go /go

WORKDIR /sources
COPY . .

RUN go build -ldflags "-w -linkmode external -extldflags \"-static\"" -o /bin/rest .

# STAGE final: And form the image it self
FROM alpine:3.13.5
COPY --from=build /bin/rest /bin/rest

RUN apk add --no-cache ca-certificates
RUN update-ca-certificates

ENV ADDR=0.0.0.0
ENV PORT=3000
EXPOSE 3000

ENTRYPOINT ["/bin/rest"]


