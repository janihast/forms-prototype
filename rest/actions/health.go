package actions

import (
	"net/http"

	"github.com/labstack/echo"
)

// Health handler
// Idea of this health function is to tell Kubernetes/docker that service is up'n'running. It should not check if database connection etc is ok. That would be health check for different layer.
func healthHandler(c echo.Context) error {
	return c.String(http.StatusOK, "OK!")
}
