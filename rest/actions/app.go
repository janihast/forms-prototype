package actions

import (
	"os"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/pkg/errors"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

// @title WireGit Forms API
// @version 1.0
// @description This API documentation for WireGit Forms API
// @termsOfService http://wiregit.com/terms/

// @contact.name API Support
// @contact.url http://wiregit.com/support

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host petstore.swagger.io
// @BasePath /v2
func App() (*echo.Echo, error) {
	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Routes
	e.GET("/health", healthHandler)

	v1 := e.Group("/v1")
	v1.Use(SetTrackingID)
	v1.Use(Database)

	// Form
	v1.GET("/form", getFormListHandler)
	v1.GET("/form/:form", getFormHandler)
	v1.POST("/form", postFormHandler)
	v1.PUT("/form/:form", putFormHandler)
	v1.PATCH("/form/:form", patchFormHandler)
	v1.DELETE("/form/:form", deleteFormHandler)

	// Form origin
	v1.GET("/form/:form/origin", getFormOriginListHandler)
	v1.GET("/form/:form/origin/:origin", getFormOriginHandler)
	v1.POST("/form/:form/origin", postFormOriginHandler)
	v1.PUT("/form/:form/origin/:origin", putFormOriginHandler)
	v1.PATCH("/form/:form/origin/:origin", patchFormOriginHandler)
	v1.DELETE("/form/:form/origin/:origin", deleteFormOriginHandler)

	// Form entry
	v1.GET("/form/:form/entry", getFormEntryListHandler)
	v1.GET("/form/:form/entry/:entry", getFormEntryHandler)
	v1.POST("/form/:form/entry", postFormEntryHandler)
	v1.PUT("/form/:form/entry/:entry", putFormEntryHandler)
	v1.PATCH("/form/:form/entry/:entry", patchFormEntryHandler)
	v1.DELETE("/form/:form/entry/:entry", deleteFormEntryHandler)

	return e, nil
}

const (
	CONTEXT_TX             = "ctx:tx"
	CONTEXT_TRACKING_TOKEN = "ctx:tracking_token"
)

// SetTrackingID generates tracking id to track the user
func SetTrackingID(next echo.HandlerFunc) echo.HandlerFunc {
	secret := os.Getenv("TRACKING_TOKEN_SECRET")

	return func(c echo.Context) error {
		t := jwt.New(jwt.SigningMethodHS256)

		token, err := t.SignedString(secret)
		if err != nil {
			return errors.Wrap(err, "unabled to sich tracking token")
		}

		c.Set(CONTEXT_TRACKING_TOKEN, token)
		return next(c)
	}
}

// Database creates connection and sets transaction for request
// Connection to database is re-established if lost and first connection is made on first request, so no need to make dependency for database
func Database(next echo.HandlerFunc) echo.HandlerFunc {
	dsn := os.Getenv("DATABASE_DSN")

	var db *gorm.DB

	return func(c echo.Context) error {
		// Open database connection if needed
		if db == nil {
			var err error
			db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
				DisableAutomaticPing:   true,
				SkipDefaultTransaction: true,
				NamingStrategy: schema.NamingStrategy{
					SingularTable: true,
				},
				Logger: logger.Default.LogMode(logger.Silent),
			})
			if err != nil {
				return errors.Wrap(err, "unabled to open database connection")
			}
		}

		sqlDB, err := db.DB()
		if err != nil {
			return errors.Wrap(err, "unable to fetch sql.DB")
		}

		// Ping checks current connection and establishes new connection if needed
		if err := sqlDB.Ping(); err != nil {
			return err
		}

		// Put request to transaction
		return db.Transaction(func(tx *gorm.DB) error {
			c.Set(CONTEXT_TX, tx)
			return next(c)
		})
	}
}
