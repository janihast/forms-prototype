module gitlab.com/johtopilvi/wiregit/forms.git/rest

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.2
	github.com/lib/pq v1.6.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/pkg/errors v0.9.1
	github.com/swaggo/echo-swagger v1.1.0 // indirect
	gitlab.com/johtopilvi/wiregit/forms.git/model v0.0.0-20210605053308-eae9eba86e5a
	golang.org/x/crypto v0.0.0-20210503195802-e9a32991a82e // indirect
	golang.org/x/net v0.0.0-20210503060351-7fd8e65b6420 // indirect
	golang.org/x/sys v0.0.0-20210503173754-0981d6026fa6 // indirect
	golang.org/x/tools v0.1.0 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.10
)

replace gitlab.com/johtopilvi/wiregit/forms.git/model => ../model
