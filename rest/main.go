package main

import (
	"fmt"

	"gitlab.com/johtopilvi/wiregit/forms.git/rest/actions"
)

func main() {
	e, err := actions.App()
	if err != nil {
		panic(fmt.Sprintf("PANIC: %s", err.Error()))
	}

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}
