migrate:
	@docker run --rm --network=docker_local --volume ${PWD}/migrations:/migrations migrate/migrate -path=/migrations/ -database mysql://${USER}:password@mariadb/database?sslmode=disable up

test:
	@cd tests && ginkgo *

schema:
	@docker run -it --rm --network docker_local -u 1000 -v "${PWD}/schemaspy.properties:/schemaspy.properties" -v "${PWD}/output:/output" schemaspy/schemaspy:latest

psql:
	@psql -h 127.0.0.1 test
