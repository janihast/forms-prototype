package model

import (
	"time"

	"github.com/gofrs/uuid"
)

type Event struct {
	ID         uuid.UUID
	Type       string
	Exchange   string
	RoutingKey string
	Data       string
	CreatedAt  time.Time
	CreatedBy  uint64
}
type Events []Event
