package model

import (
	"time"

	"github.com/gofrs/uuid"
)

type Form struct {
	ID           uint64
	Name         string
	Installation uint64
	ExternalName uuid.UUID
	Enabled      bool
	CreatedAt    time.Time
	CreatedBy    uint64
	UpdatedAt    time.Time
	UpdatedBy    uint64
	DeletedAt    *time.Time
	DeletedBy    *uint64
}

type Forms []Form
