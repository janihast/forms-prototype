package model

import "time"

type Installation struct {
	ID        uint64
	Domain    string
	CreatedAt time.Time
	CreatedBy uint64
	UpdatedAt time.Time
	UpdatedBy uint64
	DeletedAt *time.Time
	DeletedBy *uint64
}
type Installations []Installation
