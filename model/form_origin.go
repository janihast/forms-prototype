package model

import "time"

type FormOrigin struct {
	ID        uint64
	Form      uint64
	URL       string
	CreatedAt time.Time
	CreatedBy uint64
	UpdatedAt time.Time
	UpdatedBy uint64
	DeletedAt *time.Time
	DeletedBy *uint64
}

type FormsOrigins []FormOrigin
