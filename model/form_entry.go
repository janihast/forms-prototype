package model

import "time"

type FormEntry struct {
	ID              uint64
	Form            uint64
	ClientIP        string
	ClientUserAgent string
	Data            string
	CreatedAt       time.Time
	CreatedBy       uint64
	UpdatedAt       time.Time
	UpdatedBy       uint64
	DeletedAt       *time.Time
	DeletedBy       *uint64
}

type FormsEntries []FormEntry
