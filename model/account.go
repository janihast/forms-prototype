package model

type Account struct {
	ID         uint64
	ExternalID string
	Email      string
}
type Accounts []Account
