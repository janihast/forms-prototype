https://krew.sigs.k8s.io/docs/user-guide/setup/install/
kubectl create ns wg-forms
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mariadb bitnami/mariadb -n wg-forms

# Database stuff

user: root
password: $(kubectl get secret --namespace wg-forms mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 --decode)
address: mariadb.wg-forms.svc.cluster.local


# Installation

Idea is that there is installation which means basicly one bought product. Everything needs to be under that installation. And privileges goes like:

* Privilege to login is given from wiregit with a role
* Login is done with openid and role is given from there
* Installation is created on first login
* If role is admin, user can update the installation
* If role is admin, user can delete(mark deleted) the installation


https://uptrace.dev/explore/1/groups/?utm_source=pg&utm_campaign=pg-tracing&where&system=db%3Apostgresql&time_dur=3600&group_by=span.group_id&columns&sort_by=span.rate&sort_dir=desc&plot=span.rate


# TODO

* Add tracking id to events in database
* Test that you can have multiple things after delete with uniques 

# TODO: Terms

* Data deletion

# TODO: developer site

* Help for search
