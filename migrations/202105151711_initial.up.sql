CREATE TABLE form (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Record identifier',
	public_id VARCHAR(38) NOT NULL DEFAULT uuid() COMMENT 'UUID field to use for public form sending',
	enabled BOOLEAN NOT NULL DEFAULT false COMMENT 'If form accepts data in',
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the record was created',
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the record was last updated',
	deleted_at TIMESTAMP COMMENT 'If the record is deleted and when',
	CONSTRAINT UNIQUE uq_form_public_id (public_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Registered forms';

CREATE TABLE form_origin (
	id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT 'Record identifier',
	form BIGINT UNSIGNED NOT NULL COMMENT 'Reference to form',
	address VARCHAR(8192) NOT NULL COMMENT 'Allowed origin address, eg. https://example.org',
	CONSTRAINT UNIQUE uq_form_origin_form_address (form, address),
	CONSTRAINT fk_form_origin_form FOREIGN KEY (form) REFERENCES form(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'CORS origins for forms';

CREATE TABLE author (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Record identifier',
	public_id VARCHAR(38) NOT NULL DEFAULT uuid() COMMENT 'UUID field to use for public form sending',
	enabled BOOLEAN NOT NULL DEFAULT false COMMENT 'If form accepts data in',
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the record was created',
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the record was last updated',
	deleted_at TIMESTAMP COMMENT 'If the record is deleted and when',
	CONSTRAINT UNIQUE uq_form_public_id (public_id)
) ENGINE = InnoDB;

CREATE TABLE book (
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY COMMENT 'Record identifier',
	author BIGINT UNSIGNED NOT NULL,
	address VARCHAR(8192) NOT NULL COMMENT 'Allowed origin address, eg. https://example.org',
	CONSTRAINT `fk_book_author` FOREIGN KEY (author) REFERENCES author (id) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB;

CREATE UNIQUE INDEX ind_book ON book(author, address);

CREATE TABLE form_entry (
	id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	client_ip INET6 NOT NULL,
	client_user_agent VARCHAR NOT NULL,
	data JSON NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT chk_data CHECK json_valid(data)
	CONSTRAINT fk_form_origin_form FOREIGN KEY (form) REFERENCES form(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
